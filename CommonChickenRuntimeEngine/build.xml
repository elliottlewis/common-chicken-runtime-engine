<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Copyright 2013 Colby Skeggs
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 -->
<!-- This file contains the modifications used to allow the CCRE to be built
     properly and downgraded to Java 1.3 so that Squawk can use the data.-->
<project name="CommonChickenRuntimeEngine" default="default" basedir=".">
    <description>Builds, tests, and runs the project CommonChickenRuntimeEngine.</description>
    <!-- Use NetBeans' build script as the basis. -->
    <import file="nbproject/build-impl.xml"/>
    <!-- A task definition so that Retrotranslator (see the subdirectory) can be invoked here. -->
    <taskdef name="retrotranslator" classname="net.sf.retrotranslator.transformer.RetrotranslatorTask">
        <classpath>
            <fileset dir="Retrotranslator-1.2.9-bin">
                <include name="retrotranslator-transformer-1.2.9.jar" />
                <include name="retrotranslator-runtime-1.2.9.jar" />
                <include name="backport-util-concurrent-3.1.jar" />
            </fileset>
        </classpath>
    </taskdef>
    <!-- Get the properties (backport and backport.remove) that are needed to control backporting. -->
    <property file="src/backport.properties"/>
    <!-- Get the properties that specify where the sunspotfrcsdk exists. This is so that we can find the squawk jar. -->
    <property file="${user.home}/.sunspotfrc.properties"/>
    
    <!-- Once the jar is created, backport it to Java 1.3 so that Squawk can use it.
        But don't do this if there is no installation of the FRC plugins. -->
    <target name="downgrade-jar" if="sunspot.home">
        <!-- Strip out the backport.remove files so that the backporter doesn't try (and fail) to downgrade them. -->
        <jar destfile="dist\CCRE-1.5-STRIP.jar">
            <zipfileset src="dist\CCRE-1.5.jar" excludes="${backport.remove}"/>
        </jar>
        <!-- Use retrotranslator to downgrade to 1.3. -->
        <retrotranslator target="1.3" destjar="dist/CCRE.jar"
                         smart="true" verify="true" failonwarning="true" backport="${backport}">
            <jarfileset dir="dist" includes="CCRE-1.5-STRIP.jar" />
            <classpath>
                <fileset dir="${sunspot.home}/lib" includes="squawk.jar" />
            </classpath>
        </retrotranslator>
    </target>
    
    <target name="downgrade-jar-check" unless="sunspot.home">
        <echo message="Missing FRC plugin installation! Igneous projects will not work, and Javadoc will not be generated." />
    </target>
    
    <target name="-post-jar" depends="downgrade-jar,downgrade-jar-check"></target>
    
    <!-- Custom target to generate ALL CCRE javadoc, including Igneous. -->
    <target depends="init" description="Build CCRE Javadoc." name="ccre-javadoc" if="sunspot.home">
        <mkdir dir="../Javadoc"/>
        <javadoc author="Colby Skeggs" verbose="false" charset="UTF-8" destdir="../Javadoc" docencoding="UTF-8" encoding="${javadoc.encoding.used}" failonerror="true" noindex="${javadoc.noindex}" nonavbar="${javadoc.nonavbar}" notree="${javadoc.notree}" private="${javadoc.private}" source="${javac.source}" splitindex="${javadoc.splitindex}" use="${javadoc.use}" useexternalfile="true" version="${javadoc.version}" windowtitle="${javadoc.windowtitle}">
            <classpath>
                <path path="${javac.classpath}"/>
                <fileset dir="${sunspot.home}/lib">
                    <patternset>
                        <include name="**/*.jar"/>
                    </patternset>
                </fileset>
            </classpath>
            <fileset dir="${src.dir}" excludes="*.java,${excludes}" includes="${includes}">
                <filename name="**/*.java"/>
            </fileset>
            <fileset dir="../CCRE_Igneous/src" excludes="*.java">
                <filename name="**/*.java"/>
            </fileset>
            <fileset dir="${build.generated.sources.dir}" erroronmissingdir="false">
                <include name="**/*.java"/>
                <exclude name="*.java"/>
            </fileset>
        </javadoc>
    </target>
</project>

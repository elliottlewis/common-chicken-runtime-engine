/*
 * Copyright 2013 Colby Skeggs
 * 
 * This file is part of the CCRE, the Common Chicken Runtime Engine.
 * 
 * The CCRE is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * The CCRE is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the CCRE.  If not, see <http://www.gnu.org/licenses/>.
 */
package ccre.cluck;

import ccre.event.EventConsumer;
import ccre.log.LogLevel;
import ccre.log.Logger;
import ccre.log.NetworkAutologger;
import ccre.net.Network;

/**
 * A simple standalone cluck server for testing.
 *
 * @author skeggsc
 */
public class StandaloneCluckServer {

    /**
     * Start the simple server.
     *
     * @param args The program arguments. These are currently ignored.
     */
    public static void main(String[] args) {
        final long time = System.currentTimeMillis();
        CluckGlobals.ensureInitializedCore();
        NetworkAutologger.register();
        CluckGlobals.node.publish("status-report", new EventConsumer() {
            public void eventFired() {
                StringBuilder b = new StringBuilder("Standalone server online on [");
                for (String addr : Network.listIPv4Addresses()) {
                    b.append(addr).append(", ");
                }
                b.setLength(b.length() - 2);
                Logger.info(b.append("] - uptime ").append((System.currentTimeMillis() - time) / 1000).append(" seconds.").toString());
            }
        });
        CluckGlobals.setupServer();
        Logger.info("Server is running.");
        Logger.log(LogLevel.INFO, "Remote logging appears to work!", (Throwable) null);
    }
}
